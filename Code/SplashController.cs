﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SplashController : MonoBehaviour
{
    public CanvasGroup Canvas;
    public Transform TextTransform;
    private SceneController sceneController;
    void Start()
    {
        Canvas = GameObject.Find("Canvas").GetComponent<CanvasGroup>();
        Canvas.alpha = 0; // Hide it initially
        sceneController = GameObject.Find("SceneManager").GetComponent<SceneController>();
        StartCoroutine(RunSplash());
    }

    private IEnumerator RunSplash()
    {
        yield return new WaitForSeconds(2);
        Vector3 targetSize = new Vector3(1.1f, 1.1f , 1f);
        // Fade text in
        for (float f = 0; f <= 1; f += 0.01f)
        {
            Canvas.alpha = f;
            TextTransform.localScale = Vector3.Slerp(Vector3.one, targetSize, f);
            yield return new WaitForSeconds(0.02f);
        }
        yield return new WaitForSeconds(2); // Wait
        sceneController.StartScene(1);
    }

}
