﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBossLevel : MonoBehaviour
{
    private SceneController sceneController;
    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.Find("SceneManager").GetComponent<SceneController>();
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Equals("Player"))
        {
            sceneController.StartScene(9);
        }
    }
}
