﻿using UnityEngine;

public class CinematicController : MonoBehaviour
{

    private GameObject player;
    private CameraController cameraController;
    private bool isSetup = false;
    private bool shouldFinishOnStart = false;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerHealth>().Hide();
        cameraController = GameObject.Find("First-person Camera").GetComponent<CameraController>();
        isSetup = true;
        if (shouldFinishOnStart) AnimationFinished();
    }
    public void AnimationFinished()
    {
        if (!isSetup)
        {
            // This can be called from Awake(), before our variables have been initialised (even if we change Start() to Awake() above). If that is the case delay this function until Start() is called.
            shouldFinishOnStart = true;
            return;
        }

        player.GetComponent<CrosshairController>().IsCinematic = false;
        player.GetComponent<PlayerAbilityController>().IsCinematic = false;
        player.GetComponent<PlayerMovement>().IsCinematic = false;
        player.GetComponent<WeaponController>().IsCinematic = false;
        player.GetComponent<PlayerHealth>().IsPaused = false;
        player.GetComponent<PlayerHealth>().Show();
        cameraController.IsCinematic = false;
        gameObject.SetActive(false);
    }

    public void SaveAfterCinematic()
    {
        SaveGameController.SaveGame.AutoSave();
    }
}
