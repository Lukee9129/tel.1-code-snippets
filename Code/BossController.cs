﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vector3 = UnityEngine.Vector3;

public class BossController : MonoBehaviour
{
    public GameObject Boss;
    public Transform[] BigGuns;
    public Transform[] SmallGuns;
    public GameObject BigProjectile;
    public GameObject SmallProjectile;
    public AudioClip ForcefieldUp;
    public AudioClip ForcefieldDown;
    public AudioClip DeathAudio;
    public GameObject DeathExplosion;

    private GameObject revealDoor;
    private GameObject revealDoor2;
    private GameObject playerCamera;
    private GameObject player;
    private Collider forcefieldCollider;
    private ParticleSystem forcefieldPS;
    private AudioSource forcefieldAS;
    private WeaponController weaponController;
    private CameraController cameraController;
    private PlayerMovement playerMovement;
    private PlayerAbilityController playerAbilityController;
    private Animator animator;
    private EnemyController bossEnemyController;

    private int maxHealth;
    private int currentHealth;
    private bool isImmortal = true;
    private Canvas healthCanvas;
    private Slider bossHealth;
    private AudioSource bossBlare;

    public int Phase = 1;
    // Phase 1 ads
    private List<EnemyController> phase1Ads = new List<EnemyController>();

    // Phase 3 ads
    private List<EnemyController> phase3Ads = new List<EnemyController>();

    // Start is called before the first frame update
    void Start()
    {
        // Lazily populate ad lists
        GameObject phase1 = GameObject.Find("Phase 1");
        foreach (Transform ad in phase1.transform)
        {
            EnemyController enemyController = ad.gameObject.GetComponent<EnemyController>();
            phase1Ads.Add(enemyController);
            enemyController.FOV = 0; // Stop them shooting during intro
        }

        GameObject phase3 = GameObject.Find("Phase 3");
        foreach (Transform ad in phase3.transform)
        {
            EnemyController enemyController = ad.gameObject.GetComponent<EnemyController>();
            phase3Ads.Add(enemyController);
            enemyController.FOV = 0;
        }

        GameObject forcefield = GameObject.Find("Forcefield");
        forcefieldCollider = forcefield.GetComponent<Collider>();
        forcefieldPS = forcefield.GetComponent<ParticleSystem>();
        forcefieldAS = forcefield.GetComponent<AudioSource>();

        revealDoor = GameObject.Find("WallReveal");
        revealDoor2 = GameObject.Find("WallReveal2");
        playerCamera = GameObject.Find("First-person Camera");
        player = GameObject.Find("Player");
        weaponController = player.GetComponent<WeaponController>();
        cameraController = playerCamera.GetComponent<CameraController>();
        playerMovement = player.GetComponent<PlayerMovement>();
        playerAbilityController = player.GetComponent<PlayerAbilityController>();
        cameraController.IsCinematic = true;
        weaponController.IsCinematic = true;
        playerMovement.IsCinematic = true;
        playerAbilityController.IsCinematic = true;

        healthCanvas = GameObject.Find("FloatingHealth").GetComponent<Canvas>();
        healthCanvas.enabled = false;
        bossHealth = GameObject.Find("BossHealthBar").GetComponent<Slider>();
        maxHealth = PlayerPrefs.GetInt("Difficulty", 0) == 0 ? 10000 : 15000;
        currentHealth = maxHealth;
        animator = Boss.GetComponent<Animator>();
        bossBlare = GameObject.Find("BlareHorn").GetComponent<AudioSource>();
        bossEnemyController = Boss.GetComponent<EnemyController>();

        bossEnemyController.FireDelay = 5;

        SaveGameController.SaveGame.AutoSave();

        StartEncounter();
    }

    // Update is called once per frame
    void Update()
    {
        // Check if we should change phase
        if (Phase == 1)
        {
            // Remove dead ads from list
            phase1Ads.RemoveAll(go => go == null);
            if (phase1Ads.Count > 0) return; // Return if phase not complete
            StartPhase2();
            Phase++; // Start phase 2
        }
        if (Phase == 2)
        {
            if (currentHealth > maxHealth / 2) return; // Return if phase not complete
            StartPhase3();
            Phase++;
        }

        if (Phase == 3)
        {
            phase3Ads.RemoveAll(go => go == null);
            if (phase3Ads.Count > 0) return; // Return if phase not complete
            StartPhase4();
            Phase++;
        }

        if (Phase == 4)
        {
            if (currentHealth > 0) return; // Return if phase not complete
            Phase++;
            Death(); // Player wins
        }
    }

    private void StartEncounter()
    {
        StartCoroutine(OpenDoor(revealDoor));
        StartCoroutine(ShakeCamera());
    }

    private void StartPhase2()
    {
        // Power down forcefield
        ParticleSystem.EmissionModule emission = forcefieldPS.emission;
        emission.enabled = false;
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[forcefieldPS.particleCount]; // Max 150 particles
        int numParticles = forcefieldPS.GetParticles(particles);
        for (int i = 0; i < numParticles; ++i)
        {
            particles[i].remainingLifetime -= 12;
        }
        forcefieldPS.SetParticles(particles);
        forcefieldAS.clip = ForcefieldDown;
        forcefieldAS.Play();
        forcefieldCollider.enabled = false;
        animator.SetTrigger("Wake");
        bossBlare.Play();
        StartCoroutine(ShowHealth());
    }

    private void StartPhase3()
    {
        isImmortal = true;
        ParticleSystem.EmissionModule emission = forcefieldPS.emission;
        emission.enabled = true;
        forcefieldAS.clip = ForcefieldUp;
        forcefieldAS.Play();
        forcefieldCollider.enabled = true;
        revealDoor2.GetComponent<AudioSource>().Play();
        StartCoroutine(OpenDoor(revealDoor2));
        foreach (EnemyController enemyController in phase3Ads)
        {
            enemyController.FOV = 180;
        }
    }

    private void StartPhase4()
    {
        isImmortal = false;
        bossEnemyController.FireDelay = 0.5f;
        ParticleSystem.EmissionModule emission = forcefieldPS.emission;
        emission.enabled = false;
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[forcefieldPS.particleCount]; // Max 150 particles
        int numParticles = forcefieldPS.GetParticles(particles);
        for (int i = 0; i < numParticles; ++i)
        {
            particles[i].remainingLifetime -= 12;
        }
        forcefieldPS.SetParticles(particles);
        forcefieldAS.clip = ForcefieldDown;
        forcefieldAS.Play();
        forcefieldCollider.enabled = false;
        bossBlare.pitch = 0.75f;
    }

    private void Death()
    {
        player.GetComponent<PlayerHealth>().IsPaused = true; // Make them immortal
        Instantiate(DeathExplosion, Boss.transform.position, Quaternion.identity);
        bossBlare.loop = false; // Might as well use this for the audio since it won't need it anymore
        bossBlare.clip = DeathAudio;
        bossBlare.Play();
        StartCoroutine(WinGame());
    }

    private IEnumerator WinGame()
    {
        yield return new WaitForSeconds(2f); // Wait a couple seconds first
        GameObject.Find("SceneManager").GetComponent<SceneController>().StartScene(10);
    }

    private IEnumerator ShowHealth()
    {
        bossHealth.value = 0;
        healthCanvas.enabled = true;
        for (float f = 0; f < 1; f += 0.01f)
        {
            bossHealth.value = f;
            yield return new WaitForSeconds(0.01f);
        }

        isImmortal = false;
        bossEnemyController.LookDistance = 1000;
    }

    private IEnumerator OpenDoor(GameObject door)
    {
        Vector3 startLocation = door.transform.position;
        Vector3 endLocation = startLocation;
        endLocation.y = 70;
        for (float f = 0; f <= 1; f += 0.01f)
        {
            door.transform.position = Vector3.Lerp(startLocation, endLocation, f);
            yield return new WaitForSeconds(0.05f);
        }
    }

    private IEnumerator ShakeCamera()
    {
        const float range = 0.75f; // Note: range is effectively doubled
        for (int i = 0; i <= 40; ++i)
        {
            Vector3 camPos = new Vector3(Random.Range(-range, range), Random.Range(-range, range), 0.2f); // Also move it forward 0.2 so we don't see clipping with the player
            for (float f = 0; f <= 1; f += 0.25f)
            {
                playerCamera.transform.localPosition = Vector3.Lerp(Vector3.zero, camPos, f);
                yield return new WaitForSeconds(0.03f);
            }
        }
        // Now return camera to original position
        Vector3 endPos = playerCamera.transform.localPosition;
        for (float g = 0; g <= 1; g += 0.25f)
        {
            playerCamera.transform.localPosition = Vector3.Lerp(endPos, Vector3.zero, g);
            yield return new WaitForSeconds(0.03f);
        }

        // Unpause
        cameraController.IsCinematic = false;
        weaponController.IsCinematic = false;
        playerMovement.IsCinematic = false;
        playerAbilityController.IsCinematic = false;
        foreach (EnemyController enemyController in phase1Ads)
        {
            enemyController.FOV = 180;
        }
    }

    public void GetValues(out int health, out int fullHealth, out bool isBossImmortal)
    {
        health = currentHealth;
        fullHealth = maxHealth;
        isBossImmortal = isImmortal;
    }

    public void SetValues(int health)
    {
        currentHealth = health;
        bossHealth.value = currentHealth / (float)maxHealth;
    }

    public bool ShouldShoot()
    {
        return !isImmortal;
    }

    public GameObject GetNextWeaponSet(out Transform[] positions, out Vector3[] targets, out bool isSmall)
    {
        bool areWeaponsBig = Phase <= 2 || Random.Range(0, 20) == 0; // 1/20 chance of being big guns after phase 2 - wanted this to be smaller but the amount of particles, lights and projectiles causes lag (plus the fight is pretty difficult in final phase)
        GameObject projectile;
        if (areWeaponsBig)
        {
            positions = BigGuns; // Return the positions of the weapons so we can spawn projectiles from there.
            projectile = BigProjectile;
            Vector3[] targetPositions = new Vector3[SmallGuns.Length];
            // Find random places to fire above/to the sides of the player
            for (int i = 0; i < BigGuns.Length; ++i)
            {
                targetPositions[i] = new Vector3(player.transform.position.x + Random.Range(-10f, 10f), Random.Range(0, 50f), Random.Range(0, 95f));
            }
            targets = targetPositions;
        } else
        {
            positions = SmallGuns;
            Vector3[] playerPositions = new Vector3[SmallGuns.Length]; // Seems like a sub-optimal way of doing this
            for (int i = 0; i < playerPositions.Length; ++i)
                playerPositions[i] = player.transform.position;

            targets = playerPositions;
            projectile = SmallProjectile;
        }

        isSmall = !areWeaponsBig;
        return projectile;
    }
}
