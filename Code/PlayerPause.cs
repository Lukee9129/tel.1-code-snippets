﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPause : MonoBehaviour
{
    public bool IsPaused = false;
    private SceneController sceneController;
    private CrosshairController crosshairController;

    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameObject.Find("SceneManager").GetComponent<SceneController>();
        crosshairController = GetComponent<CrosshairController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsPaused)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                sceneController.PauseGame();
                // Unlock crosshair
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                crosshairController.IsPaused = true;
                IsPaused = true; // Don't allow opening of the pause menu while in the pause menu
            }
        }
    }
}