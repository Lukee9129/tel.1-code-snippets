﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

// NOTE: 'Transition' and 'StateID' is code taken from Lab 8 downloads!!
// NOTE: Code heavily inspired by Lab 8 code. 'PatrolState' and 'ChasePlayerState' have some code from EnemyBehaviour.cs

/// <summary>
/// Place the labels for the Transitions in this enum.
/// Don't change the first label, NullTransition as FSMSystem class uses it.
/// </summary>
public enum Transition
{
    NullTransition = 0, // Use this transition to represent a non-existing transition in your system
    SawPlayer = 1,
    LostPlayer = 2,
}

/// <summary>
/// Place the labels for the States in this enum.
/// Don't change the first label, NullTransition as FSMSystem class uses it.
/// </summary>
public enum StateID
{
    NullStateID = 0, // Use this ID to represent a non-existing State in your system
    PatrollingID = 1,
    ChasingPlayerID = 2,
    WanderingID = 3,
    StationaryID = 4
}

public class EnemyController : MonoBehaviour
{
    public static int HasDiedHash = Animator.StringToHash("hasDied");
    public static int HasFiredHash = Animator.StringToHash("hasFired");
    public static int HasFiredLargeHash = Animator.StringToHash("hasFiredLarge");
    public static int HasBeenShotLHash = Animator.StringToHash("hasBeenShotL");
    public static int HasBeenShotRHash = Animator.StringToHash("hasBeenShotR");
    public static int SpeedHash = Animator.StringToHash("speed");

    public int Health;
    public float DeathAnimationTime;
    public float LookDistance = 200;
    public float FOV = 60;
    public float FireDelay;
    public Transform Weapon;
    public Vector3 ProjectileSpawnOffset;
    public Vector3 SightOffset;
    public GameObject Projectile;
    public bool IsBoss = false;

    private GameObject player;
    private BossController bossController;
    private AudioSource audioSource;
    private Animator animator;
    private EnemyMovement movement;
    private int currentHealth;
    private float currentFireDelay = 0;
    private bool isDead = false;
    private bool shouldShoot = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        audioSource = GetComponent<AudioSource>();
        Random.InitState(System.DateTime.Now.Millisecond);
        currentHealth = Health;
        animator = GetComponent<Animator>();
        movement = GetComponent<EnemyMovement>();

        if (IsBoss)
            bossController = GameObject.Find("BossManager").GetComponent<BossController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead) return;
        float currentSpeed = movement.GetCurrentSpeed();
        animator.SetFloat(SpeedHash, currentSpeed);
        if (shouldShoot)
        {
            currentFireDelay += Time.deltaTime;
            if (currentFireDelay > FireDelay)
            {
                currentFireDelay = 0;
                Fire();
            }
        }
    }

    private void Fire()
    {
        if (IsBoss)
        {
            StartCoroutine(FireAfterDelay(0));
        }
        else
        {
            animator.SetTrigger(HasFiredHash);
            StartCoroutine(FireAfterDelay(0.25f)); // Waits for animation to catch up
        }
    }

    private IEnumerator FireAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (IsBoss)
        {
            Transform[] weaponPositions;
            Vector3[] weaponTargets;
            bool isSmall;
            GameObject projectile = bossController.GetNextWeaponSet(out weaponPositions, out weaponTargets, out isSmall);


            for (int i = 0; i < Mathf.Min(weaponPositions.Length, weaponTargets.Length); ++i)
            {
                Vector3 forward = weaponTargets[i] - weaponPositions[i].position;
                if (isSmall)
                {
                    animator.SetTrigger(HasFiredHash);
                    SpawnProjectile(projectile, weaponPositions[i].position, forward.normalized);
                    SpawnProjectile(projectile, weaponPositions[i].position, forward.normalized, 40); // Spawn a second slower one
                }
                else
                {
                    animator.SetTrigger(HasFiredLargeHash);
                    SpawnProjectile(projectile, weaponPositions[i].position, forward.normalized, 25);
                }
            }
        }
        else
        {
            Vector3 forward = player.transform.position - (Weapon.position + ProjectileSpawnOffset);
            SpawnProjectile(Projectile, Weapon.position + ProjectileSpawnOffset, forward.normalized);
        }
        
        audioSource.Play();
    }

    private void SpawnProjectile(GameObject projectile, Vector3 position, Vector3 forward, float velocityMultiplier = 50)
    {
        GameObject bullet = Instantiate(projectile, position, Quaternion.identity);
        bullet.GetComponent<Rigidbody>().AddForce(forward * velocityMultiplier, ForceMode.VelocityChange);
    }

    public bool TakeDamage()
    {
        if (isDead) return false;
        bool isImmortal = false;
        if (IsBoss) bossController.GetValues(out currentHealth, out Health, out isImmortal); // Ask the boss controller for values
        if (isImmortal) return false;
        currentHealth -= 25;

        if (IsBoss) bossController.SetValues(currentHealth); // Update the boss controller's values

        if (currentHealth <= 0)
        {
            animator.SetTrigger(HasDiedHash);
            movement.Kill();
            DroneController droneController = gameObject.GetComponent<DroneController>();
            if (droneController != null)
            { // Explode drones after death
                droneController.Explode();
            }
            Destroy(gameObject, DeathAnimationTime);
            isDead = true;
        }
        else
        {
            bool hitRight = Random.Range(0, 2) == 1;
            if (hitRight) animator.SetTrigger(HasBeenShotRHash);
            else animator.SetTrigger(HasBeenShotLHash);
        }
        return true;
    }

    public bool PlayerInSight(GameObject player)
    {
        Vector3 toPlayer = player.transform.position - (transform.position + SightOffset);
        float angle = Vector3.Angle(transform.forward, toPlayer);
        if (angle <= FOV)
        {
            Ray ray = new Ray(transform.position + SightOffset, toPlayer);
            // If player is not blocked by something
            RaycastHit playerHit;
            bool success = Physics.Raycast(ray, out playerHit, LookDistance) && playerHit.transform.gameObject.CompareTag("Player");
            if (IsBoss && bossController.ShouldShoot())
                shouldShoot = true;
            else
                shouldShoot = success;
            return success;
        }

        if (IsBoss && bossController.ShouldShoot())
            shouldShoot = true;
        else
            shouldShoot = false;

        return false;
    }
}

public class PatrolState : FSMState
{
    private int currentWayPoint;
    private Transform[] waypoints;
    private EnemyMovement enemyMovement;
    private float patrolSpeed = 5f;

    public PatrolState(GameObject thisObject, Transform[] wp)
    {
        waypoints = wp;
        currentWayPoint = 0;
        stateID = StateID.PatrollingID;
        enemyMovement = thisObject.GetComponent<EnemyMovement>();
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        //Check line of sight.
        if (npc.GetComponent<EnemyController>().PlayerInSight(player))
            enemyMovement.fsm.PerformTransition(Transition.SawPlayer);
        
    }


    public override void Act(GameObject player, GameObject npc)
    {
        if (Vector3.Distance(npc.transform.position, waypoints[currentWayPoint].position) <= 1)
            currentWayPoint += 1;
        

        if (currentWayPoint > waypoints.Length - 1)
            currentWayPoint = 0;


        //Update the target.
        enemyMovement.SetTarget(waypoints[currentWayPoint].position, patrolSpeed);
        enemyMovement.Move();
    }
}


public class ChasePlayerState : FSMState
{
    private EnemyMovement enemyMovement;
    private float chaseSpeed = 10f;
    private float stopDist = 10f;

    public ChasePlayerState(GameObject thisObject, GameObject tgt)
    {
        stateID = StateID.ChasingPlayerID;
        enemyMovement = thisObject.GetComponent<EnemyMovement>();
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        //Check line of sight.
        if (!npc.GetComponent<EnemyController>().PlayerInSight(player))
            enemyMovement.fsm.PerformTransition(Transition.LostPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        enemyMovement.SetTarget(player.transform.position, chaseSpeed, stopDist);
        enemyMovement.Move();
    }
}

public class WanderingState : FSMState
{
    private EnemyMovement enemyMovement;
    private float wanderSpeed = 2f;
    private Vector3 maxWanderDistance = new Vector3(10, 10, 10);
    private Vector3 startPosition;
    private Vector3? nextTarget;
    private float timeSinceWandered = 0;
    public WanderingState(GameObject thisObject, bool isFlying)
    {
        stateID = StateID.WanderingID;
        enemyMovement = thisObject.GetComponent<EnemyMovement>();
        startPosition = thisObject.transform.position;
        if (!isFlying)
            maxWanderDistance.y = 0;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        //Check line of sight.
        if (npc.GetComponent<EnemyController>().PlayerInSight(player))
            enemyMovement.fsm.PerformTransition(Transition.SawPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        timeSinceWandered += Time.deltaTime;
        if (nextTarget == null || timeSinceWandered > 10 || Vector3.Distance(npc.transform.position, nextTarget.Value) <= 1)
        {
            // Find random target to wander towards
            float maxDistanceX = maxWanderDistance.x / 2;
            float maxDistanceY = maxWanderDistance.y / 2;
            float maxDistanceZ = maxWanderDistance.z / 2;

            float wanderDistanceX = Random.Range(-maxDistanceX, maxDistanceX);
            float wanderDistanceY = Random.Range(-maxDistanceY, maxDistanceY);
            float wanderDistanceZ = Random.Range(-maxDistanceZ, maxDistanceZ);
            nextTarget = new Vector3(startPosition.x + wanderDistanceX, startPosition.y + wanderDistanceY, startPosition.z + wanderDistanceZ);
            timeSinceWandered = 0;
        }
            
        enemyMovement.SetTarget(nextTarget.Value, wanderSpeed, 0);
        enemyMovement.Move();
    }
}

public class StationaryState : FSMState
{
    private EnemyMovement enemyMovement;
    public StationaryState(GameObject thisObject)
    {
        stateID = StateID.StationaryID;
        enemyMovement = thisObject.GetComponent<EnemyMovement>();
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        //Check line of sight.
        if (npc.GetComponent<EnemyController>().PlayerInSight(player))
            enemyMovement.fsm.PerformTransition(Transition.SawPlayer);
    }

    public override void Act(GameObject player, GameObject npc)
    {
        return; // Stationary
    }
}