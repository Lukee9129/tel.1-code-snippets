﻿using System.Collections;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject[] Weapons;
    public GameObject Projectile; // TODO: Add more than one type of these when there are multiple guns
    public bool IsCinematic = false;

    private static readonly int weaponCount = 3;
    private bool[] manifestedWeapons = new bool[weaponCount];
    private int currentWeapon = 0;

    private GameObject cam;
    private AudioSource hitmarkerSound;
    private CrosshairController crosshairController;
    private TutorialController tutorialController;
    private bool isTutorial = false;
    private bool isManifestationFinished = true;

    private const float ShootDelay = 0.1f; // Prevent spam firing
    private float timeSinceLastShot = 0;
    

    void Start()
    {
        GameObject tutorialManager = GameObject.Find("TutorialManager");
        cam = GameObject.Find("First-person Camera");
        GameObject hitmarker = GameObject.Find("HitMarker");
        if (hitmarker != null)
            hitmarkerSound = hitmarker.GetComponent<AudioSource>();
        isTutorial = tutorialManager != null;
        tutorialController = tutorialManager?.GetComponent<TutorialController>();
        crosshairController = GetComponent<CrosshairController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsCinematic) return;
        if (isTutorial && !tutorialController.TutorialPartComplete) return; // Don't allow switching to weapon if we aren't at that part of the tutorial

        if (timeSinceLastShot < ShootDelay) timeSinceLastShot += Time.deltaTime;

        int nextWeapon = 0;
        if (Input.GetButton("Weapon1")) nextWeapon = 1;
        else if (Input.GetButton("Weapon2")) nextWeapon = 2;
        else if (Input.GetButton("Weapon3")) nextWeapon = 3;
        
        if (nextWeapon > 0 && nextWeapon != currentWeapon) SwitchWeapon(nextWeapon);
        if (currentWeapon != 0)
        {
            Transform weapon = Weapons[currentWeapon - 1].transform;
            weapon.rotation = Quaternion.Euler(weapon.eulerAngles.x, weapon.eulerAngles.y, -cam.transform.eulerAngles.x);
        }
    }

    // Switch currently selected weapon
    void SwitchWeapon(int id)
    {
        if (id > Weapons.Length) return;
        isManifestationFinished = true;
        int index = id - 1;

        // If weapon hasn't been manifested before, do a cool weapon spawn-in effect
        if (!manifestedWeapons[index])
        {
            ManifestWeapon(index);
            manifestedWeapons[index] = true;
        }

        // Then switch to the weapon
        for (int i = 0; i < Weapons.Length; ++i)
        {
            Weapons[i].SetActive(i == index);
        }

        currentWeapon = id;
    }

    private void ManifestWeapon(int index)
    {
        if (index != 0) return; // TODO: Only 1 gun being added for assignment.
        isManifestationFinished = false;
        GameObject weapon = Weapons[index];
        // Do fade in weapon effect
        Renderer[] childRenderers = weapon.GetComponentsInChildren<Renderer>();
        StartCoroutine(FadeInWeapon(weapon, childRenderers));
    }

    private IEnumerator FadeInWeapon(GameObject weapon, Renderer[] renderers)
    {
        Color fadeColour = new Color(0, 255, 255);

        foreach (Renderer renderer in renderers)
        {
            renderer.material.SetColor("_EmissionColor", fadeColour);
        }
        yield return new WaitForSeconds(1.5f);
        ParticleSystem.MainModule mainModule = weapon.GetComponent<ParticleSystem>().main;
        mainModule.playOnAwake = false;
        foreach (Renderer renderer in renderers)
        {
            renderer.material.SetColor("_EmissionColor", Color.black);
        }

        isManifestationFinished = true;
        // Wanted to fade between 2 colours using emission, and have parts of the weapon be transparent but don't think I can do that without a custom shader
        //        for (float f = 0; f <= 1; f += 0.01f)
        //        {
        //            foreach (Renderer renderer in renderers)
        //            {
        //                renderer.material.SetColor("_EmissionColor", Color.Lerp(fadeColour, Color.black, f));
        //            }
        //            yield return new WaitForSeconds(0.01f);
        //        }
    }

    public void FireWeapon()
    {
        if (currentWeapon != 1 || !isManifestationFinished) return; // TODO: Only 1 gun being added for assignment
        int index = currentWeapon - 1;
        GameObject weapon = Weapons[index];

        if (weapon.activeSelf)
        {
            if (timeSinceLastShot < ShootDelay) return;
            // Play gunshot audio attached to weapon
            weapon.GetComponent<AudioSource>().Play();

            // Instantiate bullet from barrel of the gun (BulletSource GO)
            Transform bulletSource = weapon.transform.Find("BulletSource");
            GameObject projectile = Instantiate(Projectile, bulletSource);
            Rigidbody rb = projectile.GetComponent<Rigidbody>();

            // Fire bullet towards centre crosshair
            Vector3 point = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, Camera.main.farClipPlane));
            Vector3 forward = Vector3.Normalize(point - transform.position);
            
            rb.AddForce(forward * 500, ForceMode.VelocityChange);
            Destroy(projectile, 1);

            LayerMask enemyMask = LayerMask.GetMask("Enemy");
            Ray ray = new Ray(transform.position, forward);

            // If crosshair is on enemy
            RaycastHit enemyHit;
            if (Physics.Raycast(ray, out enemyHit, 250, enemyMask))
            {
                // If tutorial, immediately play dead animation and destroy GO
                if (isTutorial)
                {
                    crosshairController.Hitmark();
                    Animator animator = enemyHit.transform.gameObject.GetComponent<Animator>();
                    animator.Play("dead");
                    Destroy(enemyHit.transform.gameObject, 1.5f); 
                    tutorialController.ShowInstruction(4);
                }
                else // Otherwise tell the enemy its been hit
                {
                    EnemyController controller = enemyHit.transform.gameObject.GetComponent<EnemyController>();
                    if (controller != null)
                    {
                        bool didEnemyTakeDamage = controller.TakeDamage();
                        if (didEnemyTakeDamage)
                        {
                            hitmarkerSound.Play();
                            crosshairController.Hitmark();
                        }
                    }
                }
            }
            timeSinceLastShot = 0;
        }
    }

    public void DeactivateWeapons()
    {
        foreach (GameObject weapon in Weapons)
        {
            weapon.SetActive(false);
        }

        currentWeapon = 0;
    }

}
