﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAbilityController : MonoBehaviour
{
    public bool IsCinematic = false;

    private bool IsThrowing
    {
        get => isThrowing;
        set
        {
            if (value)
            {
                hands.SetActive(true);
            }
            else
            {
                hands.SetActive(false);
            }

            isThrowing = value;
        }
    }
    private GameObject hands;
    private GameObject throwObject;
    private GameObject blinkPreview;
    private ThrowableObjectController throwController;
    private WeaponController weaponController;
    private TutorialController tutorialController;
    private PlayerMovement movement;
    private bool isTutorial;
    private bool isThrowing = false;
    private bool isBlinkValid = false;
    private Vector3 lastValidBlinkLocation;
    private float blinkCooldown = 10;
    private float timeSinceBlinked = 10;
    private GameObject blinkRegenHUD;
    private CanvasGroup blinkRegenCanvas;
    private Slider blinkBar;

    void Start()
    {
        blinkPreview = GameObject.Find("BlinkPreview");
        blinkPreview.SetActive(false);
        blinkRegenHUD = GameObject.Find("BlinkRegen");
        if (blinkRegenHUD != null)
        {
            blinkRegenCanvas = blinkRegenHUD.GetComponent<CanvasGroup>();
            blinkBar = GameObject.Find("BlinkBar").GetComponent<Slider>();
            blinkRegenHUD.SetActive(false);
        }

        hands = GameObject.Find("Hands");
        hands.SetActive(false);
        weaponController = GetComponent<WeaponController>();
        movement = GetComponent<PlayerMovement>();
        GameObject tutorialManager = GameObject.Find("TutorialManager");
        isTutorial = tutorialManager != null;
        tutorialController = tutorialManager?.GetComponent<TutorialController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsCinematic) return;

        if (Input.GetButtonDown("Throw"))
        {
            if (IsThrowing)
            {
                // If already throwing an object, deselect it
                throwController.Selected = false;
                IsThrowing = false;
                if (isTutorial && !tutorialController.TutorialPartComplete)
                    tutorialController.ShowInstruction(1);
            }
            else
            {
                // Otherwise try and find an object to throw
                GameObject throwable = FindThrowableObject();
                if (throwable != null)
                {
                    // Found throwable object, select it
                    weaponController
                        .DeactivateWeapons(); // Hide weapon in hand - in future we can use a model of hands to show we're using our 'throw' ability
                    IsThrowing = true;
                    throwObject = throwable;
                    throwController = throwable.GetComponent<ThrowableObjectController>();
                    throwController.Selected = true;
                    if (isTutorial && !tutorialController.TutorialPartComplete)
                        tutorialController.ShowInstruction(2);
                }
            }
        }


        if (timeSinceBlinked < blinkCooldown)
        {
            isBlinkValid = false;
            timeSinceBlinked += Time.deltaTime;
            if (blinkRegenHUD != null)
            {
                blinkRegenHUD.SetActive(true);
                blinkBar.value = timeSinceBlinked / blinkCooldown;
                if (timeSinceBlinked >= blinkCooldown)
                {
                    StartCoroutine(FadeBlinkRegen());
                }
            }
        }


        if (Input.GetButtonDown("Fire1"))
        {
            if (IsThrowing)
            {
                // Throw object if one is selected, object is shot away from the player
                Vector3 throwForce = Vector3.Normalize(throwObject.transform.position - transform.position);
                throwController.rb.AddForce(throwForce * 100, ForceMode.VelocityChange);
                throwController.Selected = false;
                IsThrowing = false;
                if (isTutorial)
                {
                    tutorialController.TutorialPartComplete = true; // Don't go back to this part of the tutorial under any circumstances
                    tutorialController.ShowInstruction(3);
                }
            }
            else
            {
                // Fire manifested weapon
                weaponController.FireWeapon();
            }
        }
        else if (Input.GetButton("Fire2") && timeSinceBlinked >= blinkCooldown)
        {
            if (isTutorial && !tutorialController.TutorialPartComplete) return; // Only allow blinking after the player is in the second room in the tutorial

            isBlinkValid = PreviewBlink(out lastValidBlinkLocation);
            if (isBlinkValid)
            {
                blinkPreview.SetActive(true);
                blinkPreview.transform.position = lastValidBlinkLocation;
            }
            else blinkPreview.SetActive(false);
        }

        if (Input.GetButtonUp("Fire2") && isBlinkValid)
        {
            movement.SetBlinkDestination(lastValidBlinkLocation);
            timeSinceBlinked = 0;
            blinkPreview.SetActive(false);
            if (blinkRegenHUD != null)
                blinkRegenCanvas.alpha = 1;
        }
    }

    private bool PreviewBlink(out Vector3 location)
    {
        Vector3 point = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, Camera.main.farClipPlane));
        Vector3 forward = Vector3.Normalize(point - transform.position);
        LayerMask enemyMask = LayerMask.GetMask("Floor");
        Ray ray = new Ray(transform.position, forward);

        // If crosshair is on the floor
        RaycastHit floorHit;
        if (Physics.Raycast(ray, out floorHit, 100, enemyMask))
        {
            location = floorHit.point;
            return true;
        }

        location = lastValidBlinkLocation;
        return false;
    }

    private GameObject FindThrowableObject()
    {
        LayerMask throwableMask = LayerMask.GetMask("Throwable");

        // Aim ray at centre screen (under crosshair)
        Vector3 point = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, Camera.main.nearClipPlane));
        Vector3 forward = Vector3.Normalize(point - transform.position);

        Ray ray = new Ray(transform.position, forward);

        // DEBUG:
//        Debug.DrawRay(transform.position, forward * 100, Color.green, 10);

        RaycastHit throwableHit;
        if (Physics.Raycast(ray, out throwableHit, 250, throwableMask))
            return throwableHit.transform.gameObject; // Return found object

        return null;
    }

    private IEnumerator FadeBlinkRegen()
    {
        for (float f = 0; f <= 1; f += 0.01f)
        {
            blinkRegenCanvas.alpha = Mathf.Lerp(1, 0, f);
            yield return new WaitForSeconds(0.01f);
        }

        blinkRegenHUD.SetActive(false);
    }
}