﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalityPickup : PickupController
{
    private PlayerHealth playerHealth;
    void Start()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }
    protected override void OnPickup(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            GetComponent<AudioSource>().Play();
            StartCoroutine(MakePlayerImmortal());
            Despawn(15);
        }
    }

    private IEnumerator MakePlayerImmortal() // Make the player immortal for 10 seconds
    {
        playerHealth.IsPaused = true;
        playerHealth.SetHealthTextColor(Color.cyan);
        yield return new WaitForSeconds(10);
        playerHealth.IsPaused = false;
        playerHealth.ResetHealthTextColor();
    }
}
