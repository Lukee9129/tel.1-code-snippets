﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public bool IsPaused = false;

    private GUIStyle style;
    private float painScreenValue = 1;
    private Color painColor = new Color(1, 0, 0, 0.5f);
    private Color transparentColor = new Color(1, 0, 0, 0);
    private Color healthTextColor;
    private Slider healthSlider;
    private Text healthText;
    private Canvas hud;
    void Awake()
    {
        hud = GameObject.Find("HUD").GetComponent<Canvas>();
        healthSlider = GameObject.Find("HealthBar").GetComponent<Slider>();
        healthText = GameObject.Find("HP").GetComponent<Text>();
        healthTextColor = healthText.color;
        style = new GUIStyle { normal = new GUIStyleState { background = Texture2D.whiteTexture } };
    }

    void Start()
    {
        UpdateHUD();
    }

    public void Show()
    {
        hud.enabled = true;
    }

    public void Hide()
    {
        hud.enabled = false;
    }

    public void UpdateHUD()
    {
        healthText.text = SaveGameController.SaveGame.Health.ToString();
        healthSlider.value = SaveGameController.SaveGame.Health / (float)SaveGameController.SaveGame.MaxHealth;
    }

    public void SetHealthTextColor(Color color)
    {
        healthText.color = color;
    }

    public void ResetHealthTextColor()
    {
        healthText.color = healthTextColor;
    }

    public void TakeDamage(int damage = 25)
    {
        if (IsPaused) return; // Immortality while paused
        SaveGameController.SaveGame.Health -= damage;
        UpdateHUD();
        if (SaveGameController.SaveGame.Health <= 0)
        {
            Death();
        }
        else
        {
            painScreenValue = 0;
        }
    }

    private void Death()
    {
        GameObject.Find("SceneManager").GetComponent<SceneController>().StartScene(8);
    }

    void OnGUI()
    {
        if (painScreenValue < 1)
        {
            Color prevColor = GUI.color;
            GUI.color = Color.Lerp(painColor, transparentColor, painScreenValue);
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), GUIContent.none, style);
            GUI.color = prevColor;
            painScreenValue += Time.deltaTime;
        }
    }
}
