﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBomb : MonoBehaviour
{
    public GameObject MiniProjectile;
    private float gracePeriod = 0.1f;
    private float timeSinceAlpha = 0;
    private bool hitPlayer = false; // Prevent damage being taken multiple times by the same projectile

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (timeSinceAlpha < gracePeriod) timeSinceAlpha += Time.deltaTime;
    }

    public void OnTriggerEnter(Collider col)
    {
        if (hitPlayer || timeSinceAlpha < gracePeriod) return; // We don't care about collisions within the 0.1 seconds, they are usually just colliding with the alien itself
        if (col.gameObject.name.Equals("Player"))
        {
            //Hit player
            col.gameObject.GetComponent<PlayerHealth>().TakeDamage(100);
            hitPlayer = true;
        }
        else
        {
            // Explode into smaller projectiles
            for (int i = 0; i < 50; ++i)
            { 
                Instantiate(MiniProjectile, transform.position, Quaternion.identity);
            }
        }
        Destroy(gameObject);
    }
}
