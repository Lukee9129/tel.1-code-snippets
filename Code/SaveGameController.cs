﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

// NOTE: Code inspired code by Lecture 7 notes

[Serializable]
class PlayerData
{
    public int Health;
    public int MaxHealth;
    public SerializableVector3 PlayerPosition;
    public SerializableQuaternion PlayerRotation;
    public SerializableVector3 PlayerScale;
    public int Scene;
}

// Made serializable vector3 and quaternion - they seem to work fine for now.
[Serializable]
class SerializableVector3
{
    private readonly float x;
    private readonly float y;
    private readonly float z;

    public SerializableVector3(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static implicit operator Vector3(SerializableVector3 v)
    {
        return new Vector3(v.x, v.y, v.z);
    }
    public static implicit operator SerializableVector3(Vector3 v)
    {
        return new SerializableVector3(v.x, v.y, v.z);
    }
}

[Serializable]
class SerializableQuaternion
{
    private readonly float x;
    private readonly float y;
    private readonly float z;

    public SerializableQuaternion(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static implicit operator Quaternion(SerializableQuaternion v)
    {
        return Quaternion.Euler(v.x, v.y, v.z);
    }
    public static implicit operator SerializableQuaternion(Quaternion v)
    {
        return new SerializableQuaternion(v.eulerAngles.x, v.eulerAngles.y, v.eulerAngles.z);
    }
}


public class SaveGameController : MonoBehaviour
{
    public static SaveGameController SaveGame;
    public int MaxHealth; // Prevents changing of difficulty working on save games
    public int Health;
    private int scene;
    private GameObject player;
    private PlayerData loadedData = null;
    void Awake()
    {
        if (SaveGame == null)
        {
            MaxHealth = PlayerPrefs.GetInt("Difficulty", 0) == 0 ? 200 : 100; //0 means easy
            Health = MaxHealth;
            DontDestroyOnLoad(gameObject);
            SaveGame = this;
            // Once scene has been loaded we can run SetData to set the correct values.
            SceneManager.sceneLoaded += SaveGame.SetData;
        }
        // Update player and scene when they change
        SaveGame.player = GameObject.Find("Player");
        SaveGame.scene = SceneManager.GetActiveScene().buildIndex;
        if (SaveGame != this)
        {
            Destroy(gameObject);
        }
    }

    public void AutoSave()
    {
        GameObject autoSaving = GameObject.Find("Autosaving");
        if (autoSaving != null)
        {
            Text autoSavingText = autoSaving.GetComponent<Text>();
            Color originalColour = new Color(0.6f, 0.6f, 0.6f, 1);
            autoSavingText.color = originalColour;
            Save();
            StartCoroutine(FadeAutosaving(autoSavingText, originalColour));
        }
        else
        {
            Save();
        }
    }

    private IEnumerator FadeAutosaving(Text autoSavingText, Color originalColour)
    {
        yield return new WaitForSeconds(2);
        Color noAlpha = new Color(0.6f, 0.6f, 0.6f, 0);
        for (float f = 0; f <= 1; f += 0.01f)
        {
            autoSavingText.color = Color.Lerp(originalColour, noAlpha, f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void Save()
    {
        string filename = Application.persistentDataPath + "/TEL1Save.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(filename, FileMode.OpenOrCreate);
        Transform playerTransform = player.transform;
        PlayerData pd = new PlayerData
        {
            Health = Health,
            MaxHealth = MaxHealth,
            PlayerPosition = playerTransform.position,
            PlayerRotation = playerTransform.rotation,
            PlayerScale = playerTransform.localScale,
            Scene = scene
        };
        bf.Serialize(file, pd);
        file.Close();
    }

    public int Load()
    {
        string filename = Application.persistentDataPath + "/TEL1Save.dat";
        if (File.Exists(filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filename, FileMode.Open);
            PlayerData pd = (PlayerData) bf.Deserialize(file);
            file.Close();
            loadedData = pd;
            return pd.Scene;
        }

        return -1;
    }

    // Once scene has been loaded we can run SetData to set the correct values.
    public void SetData(Scene s, LoadSceneMode mode)
    {
        if (loadedData == null) return;
        if (loadedData.Scene != s.buildIndex) return;

        Health = loadedData.Health;
        MaxHealth = loadedData.MaxHealth;
        player.transform.position = loadedData.PlayerPosition;
        player.transform.rotation = loadedData.PlayerRotation;
        player.transform.localScale = loadedData.PlayerScale;

        GameObject cinematicCamera = GameObject.Find("CinematicCamera");
        if (cinematicCamera != null)
            cinematicCamera.GetComponent<CinematicController>().AnimationFinished(); // If there is an intro cinematic, immediately end it.

        loadedData = null;
    }
}