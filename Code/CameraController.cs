﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float VertLookSpeed;
    public bool IsCinematic = false;

    void Start()
    {
        VertLookSpeed = PlayerPrefs.GetFloat("VerticalSens", 1);
    }
    void Update()
    {
        if (IsCinematic) return;
        float mouseY = -Input.GetAxis("Mouse Y");
        transform.RotateAround(transform.position, transform.right, mouseY * VertLookSpeed);
    }
}
