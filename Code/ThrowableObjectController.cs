﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableObjectController : MonoBehaviour
{
    public bool Selected
    {
        get => selected;
        set
        {
            if (value)
            {
                // TODO: Highlight selected
                // Renderer[] renderers = GetComponentsInChildren<Renderer>();
            }
            selected = value;
        }
    }
    public Rigidbody rb;

    private bool selected = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
        {
            // TODO: Make the object act as if it has been picked up by the physics gun from GMOD?
        }
    }
}
