﻿using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

// NOTE: Code heavily inspired by code provided in lecture 8 (Arrival, Pursuit, Wander methods)
public class EnemySteerMovement : EnemyMovement
{
    private Vector3? lastPosition;
    private Vector3 targetVelocity;
    private Rigidbody rb;
    private float timeSinceWandered = float.MaxValue;
    private Vector3 wanderDirection;
    protected override void AdditionalSetup()
    {
        // Since everything that uses steer movement has a rigidbody we can do this.
        rb = GetComponent<Rigidbody>();

        // If an enemy is added in future which uses transform/animation to move we'll need to change the code a bit.
    }

    protected override void UpdateDeath()
    {
        // Not needed?
    }

    protected override void UpdateMovement()
    {
        if (fsm.CurrentStateID != StateID.ChasingPlayerID)
        {
            lastPosition = null;
            targetVelocity = Vector3.zero;
            return;
        }; // Calculate velocity of player only when chasing player

        if (lastPosition != null)
            targetVelocity = (enemyTarget - lastPosition.Value) / Time.deltaTime;
        lastPosition = enemyTarget;
    }

    public override void Move()
    {
        if (isDead) return;
        
        if (fsm.CurrentStateID == StateID.WanderingID)
            Wander();
        else if (fsm.CurrentStateID == StateID.ChasingPlayerID)
            Pursuit();
        else if (fsm.CurrentStateID == StateID.PatrollingID)
            Arrival();
    }

    public override float GetCurrentSpeed()
    {
        return 1;
    }

    private void Wander()
    {
        const float stopDist = 2; // Set constant stop dist since enemyStopDist will be zero
        timeSinceWandered += Time.deltaTime;
        if (timeSinceWandered > 2)
        {
            float rotation = Random.Range(-45f, 45f);
            wanderDirection = Quaternion.Euler(0, rotation, 0) * enemyTarget; // Wander towards the randomly generated enemy target
            timeSinceWandered = 0;
        }

        Vector3 target = enemyTarget - transform.position; // Speed as we approach target (same as arrival)
        float distance = target.magnitude - stopDist;
        float slowedSpeed = enemyMoveSpeed * (distance / stopDist);
        float speed = Mathf.Min(slowedSpeed, enemyMoveSpeed);

        Vector3 wander = (wanderDirection * 5);
        rb.AddForce(wander.normalized * speed, ForceMode.Acceleration);
    }

    private void Pursuit()
    {
        if (lastPosition == null) return; // Need to wait 1 more frame so velocity has been calculated
        Vector3 target = enemyTarget - transform.position;
        if (IsFlying) target.y += 10;
        float distance = target.magnitude - enemyStopDist;
        float T = Mathf.Min(1, distance / enemyMoveSpeed);
        Vector3 advance = targetVelocity * T;
        Vector3 predictedTarget = enemyTarget + advance;

        float slowedSpeed = enemyMoveSpeed * (distance / enemyStopDist); // Slow as we approach stop dist
        float speed = Mathf.Min(slowedSpeed, enemyMoveSpeed);

        rb.AddForce((predictedTarget - transform.position).normalized * speed, ForceMode.Acceleration);
    }

    private void Arrival()
    {
        const float stopDist = 2; // Set constant stop dist since enemyStopDist will be zero
        Vector3 target = enemyTarget - transform.position;
        float distance = target.magnitude - stopDist;

        float slowedSpeed = enemyMoveSpeed * (distance / stopDist);
        float speed = Mathf.Min(slowedSpeed, enemyMoveSpeed);

        rb.AddForce(target.normalized * speed, ForceMode.Acceleration);
    }
}
