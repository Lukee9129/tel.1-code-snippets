﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeController : MonoBehaviour
{
    public enum VolumeType
    {
        Music, SFX
    }

    public VolumeType Type;
    public float DefaultVolume;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        float musicVol = PlayerPrefs.GetFloat("MusicVolume", 1);
        float sfxVol = PlayerPrefs.GetFloat("SFXVolume", 1);
        UpdateVolumes(musicVol, sfxVol);
    }

    public void UpdateVolumes(float musicVolume, float sfxVolume)
    {
        if (Type == VolumeType.Music)
        {
            audioSource.volume = DefaultVolume * musicVolume;
        }
        else if (Type == VolumeType.SFX)
        {
            audioSource.volume = DefaultVolume * sfxVolume;
        }
    }
}
