﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingLight : MonoBehaviour
{
    public float MaxIntensity = 1;
    public float MinIntensity = 0;
    public float IntensityStep = 0.5f;

    private Light theLight;
    private int direction = 1;
    private float intensity;
    // Start is called before the first frame update
    void Start()
    {
        theLight = GetComponent<Light>();
        intensity = MinIntensity;
        theLight.intensity = intensity;
    }

    void Update()
    {
        intensity += IntensityStep * direction * Time.deltaTime;
        if (intensity > MaxIntensity)
        {
            intensity = MaxIntensity;
            direction = -1;
        } else if (intensity < MinIntensity)
        {
            intensity = MinIntensity;
            direction = 1;
        }

        theLight.intensity = intensity;
    }
}
