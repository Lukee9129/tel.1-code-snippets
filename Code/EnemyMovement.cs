﻿using UnityEngine;

// Note: Code heavily inspired by Lab 8 downloads.

public abstract class EnemyMovement : MonoBehaviour
{
    public FSMSystem fsm;
    public Transform[] patrolWaypoints;
    public bool IsFlying = false;
    public bool IsBoss = false;
    public bool IsStationary = false;

    protected Vector3 enemyTarget;
    protected float enemyMoveSpeed;
    protected float enemyStopDist;
    protected bool isDead = false;
    private GameObject player;
    private bool hasRigidbody;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        hasRigidbody = GetComponent<Rigidbody>() != null;
        ChasePlayerState chaseState = new ChasePlayerState(gameObject, player);
        FSMState lostState;
        if (patrolWaypoints.Length > 0) // If we have waypoints use them
        {
            PatrolState patrolState = new PatrolState(gameObject, patrolWaypoints);
            patrolState.AddTransition(Transition.SawPlayer, chaseState.ID);
            lostState = patrolState;
        }
        else if (IsBoss || IsStationary)
        {
            StationaryState stationaryState = new StationaryState(gameObject);
            stationaryState.AddTransition(Transition.SawPlayer, chaseState.ID);
            lostState = stationaryState;
        }
        else // Otherwise just wander around
        {
            WanderingState wanderingState = new WanderingState(gameObject, IsFlying);
            wanderingState.AddTransition(Transition.SawPlayer, chaseState.ID);
            lostState = wanderingState;
        }
        
        chaseState.AddTransition(Transition.LostPlayer, lostState.ID);

        fsm = new FSMSystem();
        fsm.AddState(lostState);
        fsm.AddState(chaseState);
        AdditionalSetup();
    }

    protected abstract void AdditionalSetup();

    // Update is called once per frame
    void Update()
    {
        if (hasRigidbody || isDead) return;
        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);
    }

    void FixedUpdate()
    {
        if (!hasRigidbody || isDead) return;
        fsm.CurrentState.Reason(player, gameObject);
        fsm.CurrentState.Act(player, gameObject);
    }

    public void SetTarget(Vector3 target, float moveSpeed, float stopDist = 0)
    {
        if (isDead) return;
        enemyTarget = target;
        enemyMoveSpeed = moveSpeed;
        enemyStopDist = stopDist;
        UpdateMovement();
    }

    public void Kill()
    {
        isDead = true;
        UpdateDeath();
    }

    protected abstract void UpdateDeath();

    protected abstract void UpdateMovement();

    public abstract void Move();

    // Speed out of 1, where 0 = idle and 1 = max speed
    public abstract float GetCurrentSpeed();
}
