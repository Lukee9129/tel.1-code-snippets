﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBulletHoming : MonoBehaviour
{
    private GameObject player;
    private float gracePeriod = 0.1f;
    private float searchTime = 1; // How long the projectile spends "searching" for the target
    private float timeSinceAlpha = 0;
    private bool hitPlayer = false; // Prevent damage being taken multiple times by the same projectile
    private Rigidbody rb;

    void Start()
    {
        player = GameObject.Find("Player");
        rb = GetComponent<Rigidbody>();
        transform.rotation = Random.rotation;
        rb.AddForce(Random.insideUnitSphere.normalized * 5, ForceMode.VelocityChange);
    }
    void Update()
    {
        if (timeSinceAlpha < searchTime) timeSinceAlpha += Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (timeSinceAlpha < searchTime) return;
        Vector3 difference = player.transform.position - transform.position;
        
        // Speed up when getting to the target, and care less about previous velocity - we're now trying to orbit instead of flying straight past - this might make it too difficult.
        rb.AddForce((difference.normalized * Mathf.Min(50, 25/difference.magnitude)) - rb.velocity * 0.05f/difference.magnitude, ForceMode.Impulse);
        //rb.AddForce(difference.normalized * 5, ForceMode.Impulse); // Easier version

    }

    public void OnTriggerEnter(Collider col)
    {
        if (hitPlayer || timeSinceAlpha < gracePeriod || col.CompareTag("HomingBullet")) return; // We don't care about collisions within the 0.1 seconds, they are usually just colliding with the wall
        if (col.gameObject.name.Equals("Player"))
        {
            //Hit player
            col.gameObject.GetComponent<PlayerHealth>().TakeDamage();
            hitPlayer = true;
        }
        Destroy(gameObject);
    }
}
