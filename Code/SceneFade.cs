﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SceneFade : MonoBehaviour
{
    public Texture FadeTexture;
    private int direction = 0;
    private float alpha = 0;
    private bool fading = false;
    private readonly Stack<Action> callbacks = new Stack<Action>();

    public void FadeIn(Action callback = null)
    {
        alpha = 1;
        direction = -1;
        fading = true;
        if (callback != null)
            callbacks.Push(callback);
    }

    public void FadeOut(Action callback = null)
    {
        alpha = 0;
        direction = 1;
        fading = true;
        if (callback != null)
            callbacks.Push(callback);
    }

    void OnGUI()
    {
        if (fading)
        {
            alpha += direction * 0.5f * Time.unscaledDeltaTime;
            if (alpha >= 1 || alpha <= 0)
            {
                fading = false;
            }
            alpha = Mathf.Clamp(alpha, 0, 1);
        }
        else if (callbacks.Count > 0)
        {
            // Run all callbacks now that fading has completed
            while (callbacks.Count > 0)
                callbacks.Pop().Invoke();
        }
        Color prevColor = GUI.color;
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = -1000;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeTexture);
        GUI.color = prevColor;
    }
}
