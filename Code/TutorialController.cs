﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public Text[] Instructions;

    public bool TutorialPartComplete = false;
    // Start is called before the first frame update
    void Start()
    {
        ShowInstruction(1);
    }

    // Show the instruction text
    public void ShowInstruction(int number)
    {
        if (number < 1 || number > Instructions.Length) return;

        foreach (Text instruction in Instructions)
        {
            instruction.enabled = false;
        }

        Instructions[number - 1].enabled = true;
    }
}
