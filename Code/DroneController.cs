﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    public GameObject Explosion;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Stabilisation, slows as it approaches
        rb.MoveRotation(Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0), 0.5f));
    }

    public void Explode()
    {
        GameObject explosion = Instantiate(Explosion, transform.position, Quaternion.identity);
        ParticleSystem.MainModule pm = explosion.GetComponent<ParticleSystem>().main;
        Destroy(explosion, pm.duration);
    }
}
