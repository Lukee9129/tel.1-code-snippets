﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    private SceneFade sceneFade;
    private GameObject player;
    private CameraController cameraController;
    private MusicController musicController;
    private bool isTutorial = false;
    private bool isBossFight = false;
    private bool isFading = false;
    private bool isPauseOptionsOpen = false;

    void Start()
    {
        musicController = GetComponent<MusicController>();
        sceneFade = GetComponent<SceneFade>();
        player = GameObject.Find("Player");
        if (player == null)
        {
            // Assume this is a menu, unlock mouse.
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        cameraController = GameObject.Find("First-person Camera")?.GetComponent<CameraController>();
        isTutorial = GameObject.Find("TutorialManager") != null; // If there is a tutorial manager then its the tutorial
        isBossFight = GameObject.Find("BossManager") != null; // If there is a boss manager then it is the boss fight.
        if (isTutorial || isBossFight)
        {
            // Change exit button to not say 'save' since you can't save in the tutorial or boss fight
            GameObject exitButton = GameObject.Find("ExitButton");
            if (exitButton != null) exitButton.GetComponentInChildren<Text>().text = "Exit";
        }
        
        sceneFade.FadeIn(); // Always fade in new scenes
        SetAudioSourceVolumes(); // Update volumes based on settings
        GameObject sun = GameObject.Find("Sun");
        if (sun != null) 
            sun.GetComponent<Light>().intensity = PlayerPrefs.GetFloat("Brightness", 0.25f); //Set brightness based on settings
    }

    private void SetAudioSourceVolumes()
    {
        float musicVol = PlayerPrefs.GetFloat("MusicVolume", 1);
        float sfxVol = PlayerPrefs.GetFloat("SFXVolume", 1);
        GameObject[] audioSources = GameObject.FindGameObjectsWithTag("AudioSource");
        foreach (GameObject audioSource in audioSources)
        {
            audioSource.GetComponent<VolumeController>().UpdateVolumes(musicVol, sfxVol);
        }
    }

    public void ResumeGameAndCloseOptions()
    {
        isPauseOptionsOpen = true;
        ResumeGame();
    }

    public void EndMusic()
    {
        if (MusicController.Music != null)
            MusicController.Music.Destroy();
    }

    public void ResumeGame()
    {
        if (player != null) // Cheat and use cinematic mode to enable everything
        {
            player.GetComponent<CrosshairController>().IsCinematic = false;
            player.GetComponent<PlayerAbilityController>().IsCinematic = false;
            player.GetComponent<PlayerMovement>().IsCinematic = false;
            player.GetComponent<WeaponController>().IsCinematic = false;

            player.GetComponent<PlayerHealth>().IsPaused = false;
            player.GetComponent<PlayerPause>().IsPaused = false;
            player.GetComponent<CrosshairController>().IsPaused = false;
        }
        if (cameraController != null)
        {
            cameraController.IsCinematic = false;
        }

        GameObject[] uiToHide = GameObject.FindGameObjectsWithTag("HideableCanvas");
        foreach (GameObject g in uiToHide)
        {
            g.GetComponent<Canvas>().enabled = true;
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        SceneManager.UnloadSceneAsync(4); //Unload pause menu if its open
        if (isPauseOptionsOpen)
            SceneManager.UnloadSceneAsync(7); //Unload options menu if its open
    }

    public void PauseGame()
    {
        if (player != null) // Cheat and use cinematic mode to disable everything
        {
            player.GetComponent<CrosshairController>().IsCinematic = true;
            player.GetComponent<PlayerAbilityController>().IsCinematic = true;
            player.GetComponent<PlayerMovement>().IsCinematic = true;
            player.GetComponent<WeaponController>().IsCinematic = true;

            player.GetComponent<PlayerHealth>().IsPaused = true;
            player.GetComponent<CrosshairController>().IsPaused = true;
        }
        if (cameraController != null)
        {
            cameraController.IsCinematic = true;
        }

        GameObject[] uiToHide = GameObject.FindGameObjectsWithTag("HideableCanvas");
        foreach (GameObject g in uiToHide)
        {
            g.GetComponent<Canvas>().enabled = false;
        }

        SceneManager.LoadScene(4, LoadSceneMode.Additive);
        Time.timeScale = 0;
        //        StartCoroutine(FadeAndLoad(4, true)); - Had to change this as fading out with timeScale causes lots of issues, see comments in 'FadeAndLoad' about isPaused = true
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void SaveAndExit()
    {
        Time.timeScale = 1;
        if (!isTutorial && !isBossFight) // Don't save the tutorial
        {
            SaveGameController.SaveGame.Save();
        }
        StartCoroutine(FadeAndLoad(1)); // Go to menu
    }

    public void ResetSceneSettings()
    {
        // For when a new game has been started from the game over scenes
        SaveGameController.SaveGame.Health = SaveGameController.SaveGame.MaxHealth;
    }

    public void LoadGame()
    {
        // Launch the scene, then SaveGameController will handle the data on scene loaded.
        int sceneIndex = SaveGameController.SaveGame.Load();
        if (sceneIndex == -1) return;

        StartScene(sceneIndex);
    }

    public void StartScene(int index)
    {

        StartCoroutine(FadeAndLoad(index));
    }


    public void OpenPauseOptions()
    {
        isPauseOptionsOpen = true;
        Time.timeScale = 1;
        GameObject.Find("PauseCanvas").SetActive(false); // Hide underlying pause menu so we can display options menu on top
        SceneManager.LoadScene(7, LoadSceneMode.Additive);
        Time.timeScale = 0;
        //        StartCoroutine(FadeAndLoad(7, true)); - Had to change this as fading out with timeScale causes lots of issues, see comments in 'FadeAndLoad' about isPaused = true
    }

    private IEnumerator FadeAndLoad(int index, bool isPaused = false)
    {
        if (isFading) yield break; // Don't fade multiple times for multiple clicks
        isFading = true;
        // Fade out
        sceneFade.FadeOut();
        // Start scene
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(index, isPaused ? LoadSceneMode.Additive : LoadSceneMode.Single);
        /*
         * isPaused = true, originally was used to pause and unpause the game behind the pause menu using a stack of callbacks
         * however, this caused a couple of issues:
         * 1. the game would still move in the background while the pause menu faded in (as timeScale was 1), and then pause after the fade had completed (timeScale 0) - this looked weird as enemies could still attack you while the pause menu was opening
         * 2. If another scene was opened while the pause menu / options menu scene was fading in, occasionally the fadeout to the second scene would freeze as the callback from fading in the first scene would set timeScale to 0 - this would freeze the game permanently.
         * Because of this there is no fade out to the pause and options menus, and isPaused is never actually set to true anymore.
         */
        if (isPaused)
            sceneFade.FadeIn(() =>
            {
                Time.timeScale = 0; // Pause the game in the background after fading
                isFading = false;
            });
        else
            sceneFade.FadeIn(() => { isFading = false; });
    }

//    private IEnumerator Fade()
//    {
//        sceneFade.FadeOut();
//        yield return new WaitForSeconds(1);
//        sceneFade.FadeIn();
//    }
}