﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerMovement : MonoBehaviour
{
    public float Speed;
    public float StrafeSpeed;
    public float JumpSpeed;
    public float Gravity;
    public float HorLookSpeed;
    public bool IsCinematic = false;

    private float currentSpeed;
    private float currentStrafeSpeed;
    private Vector3 blinkDestination = Vector3.zero;
    private bool isBlinking = false;
    private float blinkDestinationThreshold = 5;

    private float currentJump = 0;

    private CharacterController cc;
    private AudioSource jumpSound;
    private AudioSource runSound;
    private AudioSource walkSound;
    // Start is called before the first frame update
    void Start()
    {
        jumpSound = GameObject.Find("Jump").GetComponent<AudioSource>();
        runSound = GameObject.Find("Footstep_Run").GetComponent<AudioSource>();
        walkSound = GameObject.Find("Footstep_Walk").GetComponent<AudioSource>();
        cc = GetComponent<CharacterController>();
        currentSpeed = Speed;
        currentStrafeSpeed = StrafeSpeed;
        HorLookSpeed = PlayerPrefs.GetFloat("HorizontalSens", 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsCinematic) return;
        // Rotate based on mouse position
        float mouseX = Input.GetAxis("Mouse X");
        transform.RotateAround(transform.position, transform.up, mouseX * HorLookSpeed);
        // NOTE: We rotate the player for horizontal look and the camera for vertical look

        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        bool jump = Input.GetButton("Jump");

        Vector3 movement = new Vector3(horizontal * currentStrafeSpeed, currentJump, vertical * currentSpeed); // Allow movement in the air
        bool isRunning = Input.GetButton("Run");
        if (cc.isGrounded)
        {
            if (isRunning) // Running can only start on the ground
            {
                currentStrafeSpeed = StrafeSpeed * 1.2f;
                currentSpeed = Speed * 1.2f;
            }
            else
            {
                currentStrafeSpeed = StrafeSpeed;
                currentSpeed = Speed;
            }

            if (jump)
            {
                // Only allow jumping on the ground
                // Randomise pitch slightly
                jumpSound.pitch = Random.Range(0.88f, 0.92f);
                jumpSound.Play();
                movement.y = JumpSpeed;
            }
            
        }

        if (!Mathf.Approximately(vertical, 0) || !Mathf.Approximately(horizontal, 0))
        {
            AudioSource movementSound;
            if (isRunning) movementSound = runSound;
            else movementSound = walkSound;

            if (!movementSound.isPlaying) movementSound.Play();
        }
        else
        {
            runSound.Stop();
            walkSound.Stop();
        }

        movement.y -= Gravity * Time.deltaTime; // Manually apply gravity since no rigidbody

        currentJump = movement.y; // Save y so it can be gradually decreased by gravity


        Vector3 add = Vector3.zero;

        if (isBlinking)
        {
            if (Vector3.Distance(blinkDestination, transform.position) <= blinkDestinationThreshold) isBlinking = false;
            else
            {
                add = blinkDestination - transform.position;
                add = add.normalized * 500;
            }
        }

        
        cc.Move((transform.TransformDirection(movement) + add) * Time.deltaTime); // Transform direction to take rotation into account then move
    }

    public void SetBlinkDestination(Vector3 destination)
    {
        blinkDestination = destination;
        isBlinking = true;
    }
}
