﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : PickupController
{
    public int HealthValue = 25;

    protected override void OnPickup(Collider col)
    {
        if (col.gameObject.CompareTag("Player") && SaveGameController.SaveGame.Health < SaveGameController.SaveGame.MaxHealth)
        {
            SaveGameController.SaveGame.Health = Mathf.Min(SaveGameController.SaveGame.MaxHealth, SaveGameController.SaveGame.Health + HealthValue);
            col.gameObject.GetComponent<PlayerHealth>().UpdateHUD();
            GetComponent<AudioSource>().Play();
            Despawn(1);
        }
    }
}
