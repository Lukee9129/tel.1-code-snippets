﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairController : MonoBehaviour
{
    public bool IsCinematic = false;
    public bool IsPaused = false;
    private GUIStyle crosshairStyle;
    private float red = 0;
    // Start is called before the first frame update
    void Start()
    {
        // Lock and hide cursor as we're using a crosshair
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        crosshairStyle = new GUIStyle { normal = new GUIStyleState { background = Texture2D.whiteTexture } };
    }
    
    void OnGUI()
    {
        if (!IsCinematic && !IsPaused)
        {
            // Draw crosshair
            Color prevColor = GUI.color;
            GUI.color = new Color(red, 0.5f, 1, 0.75f);
            Vector2 centre = new Vector2(Screen.width / 2, Screen.height / 2);
            GUI.Box(new Rect(centre.x + 10, centre.y, 25, 5), GUIContent.none, crosshairStyle);
            GUI.Box(new Rect(centre.x - 30, centre.y, 25, 5), GUIContent.none, crosshairStyle);
            GUI.Box(new Rect(centre.x, centre.y + 10, 5, 25), GUIContent.none, crosshairStyle);
            GUI.Box(new Rect(centre.x, centre.y - 30, 5, 25), GUIContent.none, crosshairStyle);
            GUI.color = prevColor;
            if (red > 0) red -= 0.01f;
        }
    }

    public void Hitmark()
    {
        red = 1;
    }
}
