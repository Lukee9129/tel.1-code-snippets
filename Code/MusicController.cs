﻿using System.Collections;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public static MusicController Music;
    private AudioSource audioSource;
    void Start()
    {
        if (Music == null)
        {
            DontDestroyOnLoad(gameObject);
            Music = this;
            audioSource = GetComponent<AudioSource>();
            StartCoroutine(FadeIn());
        } else if (Music != this)
        {
            Destroy(gameObject);
        }
    }

    public void Destroy()
    {
        Music = null;
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeIn()
    {
        float currentVolume = audioSource.volume;
        audioSource.volume = 0;
        for (float f = 0; f <= 1; f += 0.01f)
        {
            audioSource.volume = Mathf.Lerp(0, currentVolume, f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private IEnumerator FadeOut()
    {
        float currentVolume = audioSource.volume;

        for (float f = 0; f <= 1; f += 0.01f)
        {
            audioSource.volume = Mathf.Lerp(currentVolume, 0, f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}