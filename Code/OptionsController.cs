﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    private Slider musicVolumeSlider;
    private Slider sfxVolumeSlider;
    private Slider brightnessSlider;
    private Slider verticalSensSlider;
    private Slider horizontalSensSlider;
    private Text musicVolume;
    private Text sfxVolume;
    private Text brightness;
    private Text verticalSens;
    private Text horizontalSens;

    private int difficulty;
    private bool difficultyChanged = false;

    void Start()
    {
        musicVolumeSlider = GameObject.Find("MusicVolumeSlider").GetComponent<Slider>();
        sfxVolumeSlider = GameObject.Find("SFXVolumeSlider").GetComponent<Slider>();
        brightnessSlider = GameObject.Find("BrightnessSlider").GetComponent<Slider>();
        verticalSensSlider = GameObject.Find("VerticalSensSlider").GetComponent<Slider>();
        horizontalSensSlider = GameObject.Find("HorizontalSensSlider").GetComponent<Slider>();
        musicVolume = GameObject.Find("MusicVolumeValue").GetComponent<Text>();
        sfxVolume = GameObject.Find("SFXVolumeValue").GetComponent<Text>();
        brightness = GameObject.Find("BrightnessValue").GetComponent<Text>();
        verticalSens = GameObject.Find("VerticalSensValue").GetComponent<Text>();
        horizontalSens = GameObject.Find("HorizontalSensValue").GetComponent<Text>();
        float musicVol = PlayerPrefs.GetFloat("MusicVolume", 1);
        float sfxVol = PlayerPrefs.GetFloat("SFXVolume", 1);
        float brightnessVal = PlayerPrefs.GetFloat("Brightness", 0.25f);
        float verticalSensVal = PlayerPrefs.GetFloat("VerticalSens", 1);
        float horizontalSensVal = PlayerPrefs.GetFloat("HorizontalSens", 1);
        musicVolumeSlider.value = musicVol;
        sfxVolumeSlider.value = sfxVol;
        brightnessSlider.value = brightnessVal;
        verticalSensSlider.value = verticalSensVal;
        horizontalSensSlider.value = horizontalSensVal;

        // Have to manually update the labels here because for some reason if the slider value = slider min then the label won't be updated on load via slider on value changed (from above).
        SetMusicVolume();
        SetSFXVolume();
        SetBrightness();
        SetVerticalSens();
        SetHorizontalSens();

        difficulty = PlayerPrefs.GetInt("Difficulty", 0); // NOTE: Difficulty cannot be changed in game!
        GameObject difficultyToggle;
        if (difficulty == 0)
            difficultyToggle = GameObject.Find("EasyDifficulty");
        else
            difficultyToggle = GameObject.Find("HardDifficulty");

        if (difficultyToggle != null)
            difficultyToggle.GetComponent<Toggle>().isOn = true;
    }

    public void SetMusicVolume()
    {
        float value = musicVolumeSlider.value;
        musicVolume.text = value.ToString("0.##");
    }

    public void SetSFXVolume()
    {
        float value = sfxVolumeSlider.value;
        sfxVolume.text = value.ToString("0.##");
    }

    public void SetBrightness()
    {
        float value = brightnessSlider.value;
        brightness.text = value.ToString("0.##");
    }

    public void SetVerticalSens()
    {
        float value = verticalSensSlider.value;
        verticalSens.text = value.ToString("0.##");
    }

    public void SetHorizontalSens()
    {
        float value = horizontalSensSlider.value;
        horizontalSens.text = value.ToString("0.##");
    }

    public void SetDifficulty(int difficultyValue)
    {
        difficultyChanged = true;
        difficulty = difficultyValue; //0 = easy, 1 = hard
    }

    public void ShowSettingsPreview()
    {
        // If using options menu in game we can preview their changes in real time when they let go of the slider
        float musicVol = musicVolumeSlider.value;
        float sfxVol = sfxVolumeSlider.value;
        float brightnessVal = brightnessSlider.value;
        GameObject[] audioSources = GameObject.FindGameObjectsWithTag("AudioSource");
        foreach (GameObject audioSource in audioSources)
        {
            audioSource.GetComponent<VolumeController>().UpdateVolumes(musicVol, sfxVol);
        }
        GameObject sun = GameObject.Find("Sun");
        if (sun != null)
            sun.GetComponent<Light>().intensity = brightnessVal; //Set brightness based on settings
    }

    public void SaveSettings()
    {
        float musicVol = musicVolumeSlider.value;
        float sfxVol = sfxVolumeSlider.value;
        float brightnessVal = brightnessSlider.value;
        float verticalSensVal = verticalSensSlider.value;
        float horizontalSensVal = horizontalSensSlider.value;

        PlayerPrefs.SetFloat("MusicVolume", musicVol);
        PlayerPrefs.SetFloat("SFXVolume", sfxVol);
        PlayerPrefs.SetFloat("Brightness", brightnessVal);
        PlayerPrefs.SetFloat("VerticalSens", verticalSensVal);
        PlayerPrefs.SetFloat("HorizontalSens", horizontalSensVal);
        PlayerPrefs.SetInt("Difficulty", difficulty);

        GameObject[] audioSources = GameObject.FindGameObjectsWithTag("AudioSource");
        foreach (GameObject audioSource in audioSources)
        {
            audioSource.GetComponent<VolumeController>().UpdateVolumes(musicVol, sfxVol);
        }
        GameObject sun = GameObject.Find("Sun");
        if (sun != null)
            sun.GetComponent<Light>().intensity = brightnessVal; //Set brightness based on settings

        GameObject playerCamera = GameObject.Find("First-person Camera");
        if (playerCamera != null)
            playerCamera.GetComponent<CameraController>().VertLookSpeed = verticalSensVal;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
            player.GetComponent<PlayerMovement>().HorLookSpeed = horizontalSensVal;

        // If difficulty changed, change max health - current health will be updated by SceneController.ResetSceneSettings() on new game load.
        if (difficultyChanged) // Note this does not affect health/max health in a savegame
            SaveGameController.SaveGame.MaxHealth = difficulty == 0 ? 200 : 100;
    }

    public void ResetSettings()
    {
        // Reset settings if we previewed them
        float musicVol = PlayerPrefs.GetFloat("MusicVolume", 1);
        float sfxVol = PlayerPrefs.GetFloat("SFXVolume", 1);
        float brightnessVal = PlayerPrefs.GetFloat("Brightness", 0.25f);
        GameObject[] audioSources = GameObject.FindGameObjectsWithTag("AudioSource");
        foreach (GameObject audioSource in audioSources)
        {
            audioSource.GetComponent<VolumeController>().UpdateVolumes(musicVol, sfxVol);
        }
        GameObject sun = GameObject.Find("Sun");
        if (sun != null)
            sun.GetComponent<Light>().intensity = brightnessVal; //Set brightness based on settings
        difficultyChanged = false;
    }
}
