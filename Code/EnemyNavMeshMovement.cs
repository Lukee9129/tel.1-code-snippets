﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavMeshMovement : EnemyMovement
{
    private NavMeshAgent agent;
    protected override void AdditionalSetup()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    
    protected override void UpdateDeath()
    {
        agent.isStopped = true;
    }

    protected override void UpdateMovement()
    {
        agent.stoppingDistance = enemyStopDist;
        agent.speed = enemyMoveSpeed;
    }

    public override void Move()
    {
        if (isDead) return;
        agent.SetDestination(enemyTarget);
    }

    public override float GetCurrentSpeed()
    {
        return agent.velocity.magnitude / agent.desiredVelocity.magnitude;
    }
}
