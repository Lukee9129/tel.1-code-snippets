﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickupController : MonoBehaviour
{
    private bool isDisabled = false;

    void Update()
    {
        transform.Rotate(new Vector3(1, 1, 1), 50 * Time.deltaTime);
    }
    void OnTriggerEnter(Collider col)
    {
        if (isDisabled) return;
        OnPickup(col);
    }

    protected void Despawn(float delay)
    {
        // Allow delay for SFX
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderers)
            r.enabled = false; //Hide it before destroying it
        
        isDisabled = true;
        Destroy(gameObject, delay);
    }

    protected abstract void OnPickup(Collider col);
}
